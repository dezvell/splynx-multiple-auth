<?php

namespace app\controllers;

use app\models\CustomerForm;
use yii;
use yii\web\Controller;

class SiteController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionFacebook($name, $email)
    {
        if (!empty($name) and  !empty($email)) {
            Yii::$app->session->remove('user');
            Yii::$app->session->set('user', ["name" => $name, "email" => $email]);
            header('Location: '.filter_var(Yii::$app->request->baseUrl . '/profile', FILTER_SANITIZE_URL));
        } else {
            Yii::$app->getSession()->setFlash('warning', Yii::t('app', 'Facebook Auth error'));
            return $this->render('facebook');
        }
    }

    public function actionGoogle($name, $email)
    {
        if (!empty($name) and  !empty($email)) {
            Yii::$app->session->remove('user');
            Yii::$app->session->set('user', ["name" => $name, "email" => $email]);
            header('Location: '.filter_var(Yii::$app->request->baseUrl . '/profile', FILTER_SANITIZE_URL));
        } else {
            Yii::$app->getSession()->setFlash('warning', Yii::t('app', 'Google Auth error'));
            return $this->render('google');
        }
    }

    public function actionProfile()
    {
        $this->getView()->title = Yii::t('app', 'Search customer');

        $model = new CustomerForm();

        $data = [
            'model' => $model,
            'customer' => Yii::$app->session->get('user')
        ];

        if ($model->load(Yii::$app->request->post())) {
            if ($model->checkParams() === false) {
                Yii::$app->getSession()->setFlash('warning', Yii::t('app', 'Empty search params!'));
            } else {
                $customer = $model->checkCustomer();
                if ($customer['result'] === "error") {
                    Yii::$app->getSession()->setFlash('warning', Yii::t('app', $customer['message']));
                } else {
                    Yii::$app->session->set('splynx_customer_id', $customer['splynx_customer_id']);
                    return $this->redirect('/portal');
                }
            }
        }

        return $this->render('profile', $data);
    }

}
