Splynx MultipleAuth Add-on
======================

Splynx MultipleAuth Add-on based on [Yii2](http://www.yiiframework.com).

REQUIREMENTS
------------

Installed [Splynx ISP Framework](https://www.splynx.com)
~~~

Create Nginx config file `/etc/nginx/sites-available/splynx-multiple-auth.addons`:
~~~

location /multiple-auth
{
        try_files $uri $uri/ /multiple-auth/index.php?$args;
}
~~~

Restart Nginx:
~~~

sudo service nginx restart
~~~

Install add-on base:
~~~
cd /var/www/splynx/addons/
git clone https://bitbucket.org/splynx/splynx-addon-base.git
cd splynx-addon-base
composer global require "fxp/composer-asset-plugin:^1.2.0"
composer install
~~~

Install Splynx MultipleAuth Add-on:
~~~
cd /var/www/splynx/addons/
git clone https://dezvell@bitbucket.org/dezvell/splynx-multiple-auth.git
cd splynx-multiple-auth
composer install
sudo chmod +x yii
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-multiple-auth/web/ /var/www/splynx/web/multiple-auth
~~~

Additional settings avaiable in `/var/www/splynx/addons/splynx-multiple-auth/config/params.php`.

You can then access Splynx MultipleAuth Add-On through following URL:
~~~
http://YOUR_SPLYNX_DOMAIN/multiple-auth
~~~
For access you must enable MultipleAuth for admin on page "Administration / Administrators" in Splynx.