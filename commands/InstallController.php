<?php

namespace app\commands;

use splynx\base\BaseInstallController;

class InstallController extends BaseInstallController
{
    public function getAddOnTitle()
    {
        return 'MultipleAuth';
    }

    public function getModuleName()
    {
        return 'splynx_multiple_auth_addon';
    }

    public function getApiPermissions()
    {
        return [
            [
                'controller' => 'api\admin\administration\Administrators',
                'actions' => ['index', 'update', 'view'],
            ],
            [
                'controller' => 'api\admin\config\Download',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
                'actions' => ['index', 'view'],
            ]
        ];
    }

    public function getAdditionalFields()
    {
        return [
            [
                'main_module' => 'admins',
                'name' => 'multiple_auth',
                'title' => 'MultipleAuth',
                'type' => 'boolean',
                'required' => false,
                'is_add' => true,
            ]
        ];
    }

    public function getEntryPoints()
    {
        return [];
    }
}
