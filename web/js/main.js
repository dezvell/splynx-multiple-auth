function statusChangeCallback(response) {
    if (response.status === 'connected') {
        FB.api('/me?fields=name,email', function(response) {
            var name = response.name;
            var email = response.email;
            location.href = '/multiple-auth/facebook?name=' + name + '&email=' + email;
        });
    }
}

function checkLoginState() {
    FB.login(function(response) {
        statusChangeCallback(response);
    }, {
        scope: 'email',
        return_scopes: true
    });
}

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    var name = profile.getName();
    var email = profile.getEmail();
    location.href = '/multiple-auth/google?name=' + name + '&email=' + email;
}