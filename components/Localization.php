<?php

namespace app\components;

use splynx\helpers\ConfigHelper;
use yii\base\BaseObject;

/**
 * Class Localization
 * @package app\components
 */
class Localization extends BaseObject
{

    /**
     * @return string
     */
    public static function getDataTablesLocaleUrl()
    {
        return '/multiple_auth/locale/' . ConfigHelper::get('language') . '.json';
    }
}
