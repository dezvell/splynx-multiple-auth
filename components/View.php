<?php

namespace app\components;

use yii\base\ViewNotFoundException;

class View extends \yii\web\View
{
    /**
     * @param $view
     * @param array $params
     * @param null $context
     * @return mixed
     */
    public function render($view, $params = [], $context = null)
    {
        try {
            $file = $view;
            if (($ext_file = pathinfo($view, PATHINFO_EXTENSION)) !== '') {
                $file = str_replace('.' . $ext_file, null, $file);
            }

            // Render *.custom.twig (SAC-14)
            $file .= '.custom.twig';
            return parent::render($file, $params, $context);
        } catch (ViewNotFoundException $e) {
            return parent::render($view, $params, $context);
        }
    }
}
