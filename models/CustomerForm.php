<?php

namespace app\models;

use Yii;
use yii\base\Model;

include dirname(__FILE__) . "/../../splynx-php-api/SplynxApi.php";

/**
 * Class CustomerForm
 * @package app\models
 */
class CustomerForm extends Model
{
    public $login;
    public $name;
    public $email;
    public $street_1;
    public $street_2;
    public $bday;
    public $password;
    public $repeat_password;

    /**
     * @var \SplynxAPI
     */
    protected $API;
    protected $API_URL = "admin/customers/customer";

    /**
     * CustomerForm constructor.
     */
    function __construct() {
        $this->API = new \SplynxAPI(Yii::$app->params['api_domain'], Yii::$app->params['api_key'],  Yii::$app->params['api_secret']);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['login', 'name', 'email', 'street_1', 'street_2', 'bday', 'password', 'repeat_password'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'login' => Yii::t('app', 'Login'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'street_1' => Yii::t('app', 'Street 1'),
            'street_2' => Yii::t('app', 'Street 1'),
            'bday' => Yii::t('app', 'Birth Day'),
            'password' => Yii::t('app', 'Password'),
            'repeat_password' => Yii::t('app', 'Repeat Password'),
        ];
    }

    /**
     * @return bool
     */
    public function checkParams()
    {
        if (($this->login == null and $this->name == null and $this->email == null and $this->street_1 == null and $this->street_2 == null and $this->bday == null and $this->password == null and $this->repeat_password == null)) {
            return false;
        }

        return true;
    }

    /**
     * @return array|bool
     */
    private function checkEmail() {
        $paramEmail = [
            'main_attributes' => []
        ];

        $paramEmail['main_attributes']['email'] = $this->email;

        $locationsApiUrl = $this->API_URL . '?' . http_build_query($paramEmail);

        $resultEmail = $this->API->api_call_get($locationsApiUrl);
        if ($resultEmail) {
            if (!empty($this->API->response[0])) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return array|bool
     */
    private function checkLogin() {
        $paramLogin = [
            'main_attributes' => []
        ];

        $paramLogin['main_attributes']['login'] = $this->login;

        $locationsApiUrl = $this->API_URL . '?' . http_build_query($paramLogin);

        $resultLogin = $this->API->api_call_get($locationsApiUrl);
        if ($resultLogin) {
            if (!empty($this->API->response[0])) {
                return false;
            }
        }

        return true;
    }

    private function sendEmail() {
        $paramEmail = [];
        $paramEmail['type'] = 'message';
        $paramEmail['message_id'] = 1;
        $paramEmail['status'] = 'sent';
        $paramEmail['datetime_added'] = date("Y-m-d H:i:s");
        $paramEmail['datetime_sent'] = date("Y-m-d H:i:s");
        $paramEmail['recipient'] = $this->email;
        $paramEmail['copy_to'] = $this->email;
        $paramEmail['subject'] = 'Reminder 1';
        $paramEmail['message'] = 'Mail message';

        $locationsApiUrl = "/admin/config/mail";

        $this->API->api_call_post($locationsApiUrl, $paramEmail);
    }

    /**
     * @return array
     */
    public function checkCustomer()
    {

        if (empty($this->login)) {
            return ['result' => 'error', 'message' => Yii::t('app', 'Login was empty')];
        }

        if (!$this->checkLogin()) {
            return ['result' => 'error', 'message' => Yii::t('app', 'this login is already taken')];
        }

        if (empty($this->email)) {
            return ['result' => 'error', 'message' => Yii::t('app', 'Email was empty')];
        }

        if (!$this->checkEmail()) {
            return ['result' => 'error', 'message' => Yii::t('app', 'this email is already taken')];
        }

        if (empty($this->password)) {
            return ['result' => 'error', 'message' => Yii::t('app', 'Password not equal')];
        }

        if ($this->password !== $this->repeat_password) {
            return ['result' => 'error', 'message' => Yii::t('app', 'Password is empty')];
        }

        $paramRegister = [];
        $paramRegister['login'] = $this->login;
        $paramRegister['status'] = 'new';
        $paramRegister['partner_id'] = 1;
        $paramRegister['location_id'] = 1;
        $paramRegister['name'] = $this->name;
        $paramRegister['email'] = $this->email;
        $paramRegister['phone'] = '';
        $paramRegister['category'] = 'person';
        $paramRegister['street_1'] = $this->street_1;
        $paramRegister['street_2'] = $this->street_2;
        $paramRegister['zip_code'] = '';
        $paramRegister['city'] = '';
        $paramRegister['date_add'] = date("Y-m-d");

        $result = $this->API->api_call_post($this->API_URL, $paramRegister);
        if ($result && !empty($this->API->response['id'])) {
            $splynx_customer_id = $this->API->response['id'];
            $this->sendEmail();
            return ['result' => 'success', 'splynx_customer_id' => $splynx_customer_id];
        } else {
            return ['result' => 'error', 'message' => Yii::t('app', $this->API->response[0]['message'])];
        }
    }
}
