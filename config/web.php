<?php

return function ($params, $baseDir) {
    return [
        'language' => isset($params['language']) ? $params['language'] : 'en-US',
        'components' => [
            'request' => [
                'baseUrl' => '/multiple-auth',
            ],
            'view' => [
                'class' => 'app\components\View',
            ],
            'i18n' => [
                'translations' => [
                    'app*' => [
                        'class' => 'yii\i18n\PhpMessageSource',
                        'basePath' => '@app/messages',
                        'sourceLanguage' => 'en-US',
                        'forceTranslation' => true,
                    ],
                ]
            ],
        ],
    ];
};
